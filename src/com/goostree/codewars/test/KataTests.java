package com.goostree.codewars.test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;

import com.goostree.codewars.NextBiggest;

public class KataTests {
    @Test
    public void basicTests() {
         assertEquals(21, NextBiggest.nextBiggerNumber(12));
         assertEquals(531, NextBiggest.nextBiggerNumber(513));
         assertEquals(2071, NextBiggest.nextBiggerNumber(2017));
         assertEquals(441, NextBiggest.nextBiggerNumber(414));
         assertEquals(414, NextBiggest.nextBiggerNumber(144));
    }      
}
