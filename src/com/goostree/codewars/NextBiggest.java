package com.goostree.codewars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NextBiggest {
	
	public static void main(String[] args) {
		System.out.println(nextBiggerNumber(59884848459853l));
	}
	
	public static long nextBiggerNumber(long x) {
        List<Integer> list = getDigits(x);
        int i;
        System.out.println(x);
        //find index of first digit smaller than its right neightbor
        for(i = 0; i < list.size() - 1; i++) {
        	if(list.get(i) > list.get(i+1)) {
        		break;
        	}
        }
        
        //if sorted in descending, no dice
    	if(i == list.size() - 1) {
    		return -1;
    	}
    	
    	//swap with the smallest (larger) value to the right of i
    	for(int j = 0; j < i + 1; j++) {
    		if(list.get(j) > list.get(i+1)) {
    			Collections.swap(list, i+1, j);
    			break;
    		}
    	}
    	
    	//sort list to right of i
        Collections.sort(list.subList(0, i+1), Collections.reverseOrder());
    	
    	return getValue(list);
    }

    /**
     * 
     * @param x - the integer to split into digits
     * @return - List<Integer> of the digits in the parameter,
     * 		from least significant to most
     */
    private static List<Integer> getDigits(long x) {
        List<Integer> list = new ArrayList<Integer>();

        while(x > 0) {

            list.add((int) (x % 10));

            x /= 10;

        }
        return list;
    }
    
    /**
     * 
     * @param list - a list of digits representing an integer,
     * 		ordered from least significant to most
     * @return - the value of the integer represented by the list
     */
    private static long getValue(List<Integer> list) {
        long result = 0;
        for(int i = 0; i < list.size(); i++) {
            result += list.get(i) * Math.pow(10, i);
        }
        return result;
    }
}
